cmake_minimum_required(VERSION 2.8.3)
project(planning_code)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
	roscpp
	rospy
	std_msgs
	moveit_ros_planning_interface
	rviz_visual_tools
	moveit_visual_tools
	tf
	tf_conversions
	std_srvs
	ur_msgs
	message_generation
	trajectory_msgs
	visualization_msgs
)

# System dependencies are found with CMake's conventions
 find_package(Boost REQUIRED COMPONENTS system)
 find_package(Eigen3)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
## add_service_files(
## 	FILES
## 	movecamera.srv
## 	moveArm.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
## generate_messages(
##   DEPENDENCIES
##   sensor_msgs
##   std_msgs
##   geometry_msgs
  
## )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################


## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES work
CATKIN_DEPENDS message_runtime roscpp rospy std_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
# add_library(work
#   src/${PROJECT_NAME}/work.cpp
# )

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
#add_dependencies(work ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
#add_executable(Motion src/ControllerFG1.cpp)
#add_executable(markerpose src/markerpose.cpp)
#add_executable(marker_detect src/marker_client.cpp)
#add_executable(camera_move_srv src/move_camera.cpp)
#add_executable(arm_motion src/Arm_Mrkr_pik_box.cpp)
#add_executable(arm_motion src/Arm_move_Mrkr.cpp)
#add_executable(move_arm src/Arm_Mrkr_pik_box.cpp)
#add_executable(attach src/attach_obj.cpp)
#add_executable(detach src/detach_obj.cpp)
#add_executable(plan_motion src/motion_planning.cpp)
add_executable(Arm_move src/Arm_move.cpp)
#add_executable(move_base src/movebase.cpp)
#add_executable(collect_data src/Lidar_data.cpp)
#add_executable(rack_navigate src/rack_navigate.cpp)
#add_executable(kdl_arm_move_srv src/kdl_arm_pos_cntrl_serv.cpp)
## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(work_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
#add_dependencies(Motion work_generate_messages_cpp)
#add_dependencies(markerpose work_generate_messages_cpp)
#add_dependencies(detach work_generate_messages_cpp)
#add_dependencies(arm_motion work_generate_messages_cpp)
#add_dependencies(attach work_generate_messages_cpp)
#add_dependencies(plan_motion work_generate_messages_cpp)
#add_dependencies(move_arm work_generate_messages_cpp)
#add_dependencies(robot_arm_move work_generate_messages_cpp)
#add_dependencies(move_base work_generate_messages_cpp)
#add_dependencies(marker_detect work_generate_messages_cpp)
#add_dependencies(collect_data work_generate_messages_cpp)
#add_dependencies(rack_navigate work_generate_messages_cpp)
#add_dependencies(camera_move_srv work_generate_messages_cpp)
#add_dependencies(kdl_arm_move_srv work_generate_messages_cpp)
## Specify libraries to link a library or executable target against
#target_link_libraries(Motion ${catkin_LIBRARIES} )
#target_link_libraries(markerpose ${catkin_LIBRARIES} )
#target_link_libraries(arm_motion ${catkin_LIBRARIES} )
#target_link_libraries(attach ${catkin_LIBRARIES} )
#target_link_libraries(detach ${catkin_LIBRARIES} )
#target_link_libraries(plan_motion ${catkin_LIBRARIES} )
#target_link_libraries(move_arm ${catkin_LIBRARIES} )
target_link_libraries(Arm_move ${catkin_LIBRARIES} )
#target_link_libraries(move_base ${catkin_LIBRARIES} )
#target_link_libraries(marker_detect ${catkin_LIBRARIES} )
#target_link_libraries(collect_data ${catkin_LIBRARIES} )
#target_link_libraries(rack_navigate ${catkin_LIBRARIES} )
#target_link_libraries(camera_move_srv ${catkin_LIBRARIES} )
#target_link_libraries(kdl_arm_move_srv ${catkin_LIBRARIES} )
#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS work work_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )


#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_work.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
