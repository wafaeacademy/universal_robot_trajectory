
	//----Arm Motion Node for Moving The Arm to the desired coordinates---//
	#include <moveit/move_group_interface/move_group_interface.h>
	#include <moveit/planning_scene_interface/planning_scene_interface.h>

	#include <moveit_msgs/DisplayRobotState.h>
	#include <moveit_msgs/DisplayTrajectory.h>

	#include <moveit_msgs/AttachedCollisionObject.h>
	#include <moveit_msgs/CollisionObject.h>
	#include <iostream>
	#include <ros/ros.h>
	#include <math.h>
	#include <moveit/trajectory_processing/iterative_time_parameterization.h>
	#define PI 3.14
	#include<iostream>
	
	using namespace std;
	
	int main(int argc, char **argv)
	{
		ros::init(argc, argv, "move_group_interface_tutorial");
		ros::NodeHandle node_handle;  
		ros::AsyncSpinner spinner(1);
		spinner.start();
		
		moveit::planning_interface::MoveGroupInterface group("manipulator");
		 
		moveit::planning_interface::PlanningSceneInterface planning_scene_interface;  
		
		ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
		
		moveit_msgs::DisplayTrajectory display_trajectory;

		ROS_INFO("Reference frame: %s", group.getPlanningFrame().c_str());
		
		ROS_INFO("Reference frame: %s", group.getEndEffectorLink().c_str());
		
		moveit::planning_interface::MoveItErrorCode error_code;
		
		// Planning to a Pose goal
		
		geometry_msgs::Pose target_pose1;
                target_pose1.orientation.x = 0.7;
                target_pose1.orientation.y = 0.7;
                target_pose1.orientation.z = -0.001;
		target_pose1.orientation.w = 0.0;
                target_pose1.position.x = 0.761;
                target_pose1.position.y = 0.16;
                target_pose1.position.z = 0.52;
		group.setPoseTarget(target_pose1);

		//Now, we call the planner to compute the plan and visualize it.
		moveit::planning_interface::MoveGroupInterface::Plan move_plan;
                //bool success = group.plan(move_plan);
                moveit::planning_interface::MoveItErrorCode success = group.plan(move_plan);
		
		ROS_INFO("Visualizing plan 1 (pose goal) %s",success?"":"FAILED");    
		/* Sleep to give Rviz time to visualize the plan. */
		sleep(10.0);
		
		 // Visualizing plans
		 
		if (1)
		{
			ROS_INFO("Visualizing plan 1 (again)");    
			display_trajectory.trajectory_start = move_plan.start_state_;
			display_trajectory.trajectory.push_back(move_plan.trajectory_);
			display_publisher.publish(display_trajectory);
			/* Sleep to give Rviz time to visualize the plan. */
			sleep(5.0);
		}
		
		//Execute Move (blocking)
		 //group.move();
		 ROS_INFO("----------EXECUTING THE MOVE to POSE 1----------");
		 error_code = group.move();
		 ROS_INFO("Execution Finished and error_code is : %d",error_code);
		 //sleep(5.0);
		 
		 //--------------------------------------------------------
		 //Sending Waypoins to move the Arm
		 
	
		  std::vector<geometry_msgs::Pose> waypoints;

			geometry_msgs::Pose target_pose2 = target_pose1;
			target_pose2.position.x += 0.2;
			target_pose2.position.z += 0.2;
			waypoints.push_back(target_pose2);  // up and out

			target_pose2.position.y -= 0.5;
			waypoints.push_back(target_pose2);  // left

			target_pose2.position.z -= 0.2;
			target_pose2.position.y += 0.5;
			target_pose2.position.x -= 0.2;
			 //sleep(5.0);
			waypoints.push_back(target_pose2);  // down and right (back to start)
			
			// We want the cartesian path to be interpolated at a resolution of 1 cm
			// which is why we will specify 0.01 as the max step in cartesian
			// translation.  We will specify the jump threshold as 0.0, effectively
			// disabling it.
			moveit_msgs::RobotTrajectory trajectory;
			double fraction = group.computeCartesianPath(waypoints,
																									 0.01,  // eef_step
																									 0.0,   // jump_threshold
																									 trajectory);
			move_plan.trajectory_=trajectory;

			ROS_INFO("Visualizing plan 2 (cartesian path) (%.2f%% acheived)",fraction * 100.0);    
			/* Sleep to give Rviz time to visualize the plan. */
			sleep(15.0);
			
			//---------------------------------------------------------------
			ROS_INFO("----------EXECUTING THE TRAJECTORY MOVE----------");
						//Execute Move (blocking)
			error_code=group.execute(move_plan);
				
			ROS_INFO("Execution Finished and error_code is : %d",error_code);
		 
			//sleep(10.0);

			//----------------------------------------------------------------------
		
		  // Planning to a Home pose
			 
			geometry_msgs::Pose target_pose0; 
			target_pose0.orientation.x = 0.707;
			target_pose0.orientation.y = -0.707;
			target_pose0.orientation.z = 0.0;
			target_pose0.orientation.w = 0.0;
			target_pose0.position.x = -1.0;
			target_pose0.position.y = 0.0;
			target_pose0.position.z = 0.5;	 
			group.setPoseTarget(target_pose0);

			success = group.plan(move_plan);
			
			cout<<"\tsuccess\t"<<success<<endl;
			
			ROS_INFO("Visualizing plan 3 (pose goal) %s",success?"":"FAILED");    
			/* Sleep to give Rviz time to visualize the plan. */
			sleep(10.0);
			
			// Visualizing plans
			 
			if (1)
			{
				ROS_INFO("Visualizing plan 3 (again)");    
				display_trajectory.trajectory_start = move_plan.start_state_;
				display_trajectory.trajectory.push_back(move_plan.trajectory_);
				display_publisher.publish(display_trajectory);
				/* Sleep to give Rviz time to visualize the plan. */
				sleep(5.0);
			}
		
			if(group.move()==moveit_msgs::MoveItErrorCodes::SUCCESS)
			 ROS_INFO("MOVE SUCCESSFUL TO HOME POSE");
			else
			{
				success = group.plan(move_plan);
				cout<<"\tsuccess\t"<<success<<endl;
				ROS_INFO("Visualizing plan 3 (pose goal) %s",success?"":"FAILED");    
			/* Sleep to give Rviz time to visualize the plan. */
				sleep(10.0);
			
			 // Visualizing plans
			 
				if (1)
				{
					ROS_INFO("Visualizing plan 3 (again)");    
					display_trajectory.trajectory_start = move_plan.start_state_;
					display_trajectory.trajectory.push_back(move_plan.trajectory_);
					display_publisher.publish(display_trajectory);
					/* Sleep to give Rviz time to visualize the plan. */
					sleep(5.0);
				}
			
			
				//Execute Move (blocking)	
				//group.move();
				 if(group.move()==moveit_msgs::MoveItErrorCodes::SUCCESS)
				 ROS_INFO("MOVE SUCCESSFUL ON SECOND TRY");
				 
				 else
				 {
						target_pose0.orientation.x = 0.0;
						target_pose0.orientation.y = 1.0;
						target_pose0.orientation.z = 0.0;
						target_pose0.orientation.w = 0.0;
						target_pose0.position.x = -0.8;
						target_pose0.position.y = 0.13;
						target_pose0.position.z = 1.3;	 
						group.setPoseTarget(target_pose0);
					 
						success = group.plan(move_plan);
						cout<<"\tsuccess\t"<<success<<endl;
						ROS_INFO("Visualizing plan 5 (pose goal) %s",success?"":"FAILED");    
						/* Sleep to give Rviz time to visualize the plan. */
						sleep(10.0);
						
						// Visualizing plans
						 
						if (1)
						{
							ROS_INFO("Visualizing plan 5 (again)");    
							display_trajectory.trajectory_start = move_plan.start_state_;
							display_trajectory.trajectory.push_back(move_plan.trajectory_);
							display_publisher.publish(display_trajectory);
							/* Sleep to give Rviz time to visualize the plan. */
							sleep(5.0);
						}
					
						if(group.move()==moveit_msgs::MoveItErrorCodes::SUCCESS)
						 ROS_INFO("MOVED TOWARDS THE HOME POSE");
						
					 
							target_pose0.orientation.x = 0.707;
							target_pose0.orientation.y = -0.707;
							target_pose0.orientation.z = 0.0;
							target_pose0.orientation.w = 0.0;
							target_pose0.position.x = -1.0;
							target_pose0.position.y = 0.0;
							target_pose0.position.z = 0.8;	 
							group.setPoseTarget(target_pose0);

							success = group.plan(move_plan);
							cout<<"\tsuccess\t"<<success<<endl;
							ROS_INFO("Visualizing plan 6 (pose goal) %s",success?"":"FAILED");    
							/* Sleep to give Rviz time to visualize the plan. */
							sleep(10.0);
							
							// Visualizing plans
							 
							if (1)
							{
								ROS_INFO("Visualizing plan 6 (again)");    
								display_trajectory.trajectory_start = move_plan.start_state_;
								display_trajectory.trajectory.push_back(move_plan.trajectory_);
								display_publisher.publish(display_trajectory);
								/* Sleep to give Rviz time to visualize the plan. */
								sleep(5.0);
							}
						
							if(group.move()==moveit_msgs::MoveItErrorCodes::SUCCESS)
							 ROS_INFO("MOVE SUCCESSFUL TO HOME POSE");
										
											 
					 }
							 
							//sleep(5.0);
			  }
				
			
			ros::shutdown();  
		return 0;
  
	}
		
		
		
