
	//----Arm Motion Node for Moving The Arm to the desired coordinates---//
	#include <moveit/move_group_interface/move_group_interface.h>
	#include <moveit/planning_scene_interface/planning_scene_interface.h>

	#include <moveit_msgs/DisplayRobotState.h>
	#include <moveit_msgs/DisplayTrajectory.h>

	#include <moveit_msgs/AttachedCollisionObject.h>
	#include <moveit_msgs/CollisionObject.h>
	#include <iostream>
	#include <ros/ros.h>
	#include <math.h>
	#include <moveit/trajectory_processing/iterative_time_parameterization.h>
        #include <moveit_visual_tools/moveit_visual_tools.h>
	#define PI 3.14
	#include<iostream>
	
	using namespace std;
	
	int main(int argc, char **argv)
	{
		ros::init(argc, argv, "move_group_interface_tutorial");
		ros::NodeHandle node_handle;  
		ros::AsyncSpinner spinner(1);
		spinner.start();
		
                // moveit::planning_interface::MoveGroupInterface group("manipulator");
                static const std::string PLANNING_GROUP = "manipulator";
                moveit::planning_interface::MoveGroupInterface group(PLANNING_GROUP);
		 
                moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

                const robot_state::JointModelGroup *joint_model_group =
                    group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

                namespace rvt = rviz_visual_tools;
                moveit_visual_tools::MoveItVisualTools visual_tools("manipulator");
                visual_tools.deleteAllMarkers();
                // Remote control is an introspection tool that allows users to step through a high level script
                // via buttons and keyboard shortcuts in RViz
                visual_tools.loadRemoteControl();

                // RViz provides many types of markers, in this demo we will use text, cylinders, and spheres
                Eigen::Affine3d text_pose = Eigen::Affine3d::Identity();
                text_pose.translation().z() = 1.75;
                /* text_pose.translation().z() = 2.75; // above head of PR2
                geometry_msgs::Vector3 scale;
                scale.x=0.3;
                scale.y=0.3;
                scale.z=0.3;*/
                visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);

                // Batch publishing is used to reduce the number of messages being sent to RViz for large visualizations
                visual_tools.trigger();

                ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);

                moveit_msgs::DisplayTrajectory display_trajectory;

                ROS_INFO("Reference frame: %s", group.getPlanningFrame().c_str());

                ROS_INFO("Reference frame: %s", group.getEndEffectorLink().c_str());

                moveit::planning_interface::MoveItErrorCode error_code;

                // Planning to a Pose goal

                geometry_msgs::Pose target_pose1;
                target_pose1.orientation.x = 0.7;
                target_pose1.orientation.y = 0.7;
                target_pose1.orientation.z = -0.001;
                target_pose1.orientation.w = 0.0;
                target_pose1.position.x = 0.761;
                target_pose1.position.y = 0.16;
                target_pose1.position.z = 0.52;
                group.setPoseTarget(target_pose1);

                //Now, we call the planner to compute the plan and visualize it.
                moveit::planning_interface::MoveGroupInterface::Plan move_plan;
                //bool success = group.plan(move_plan);
                //moveit::planning_interface::MoveItErrorCode success = group.plan(move_plan);

                bool success = (group.plan(move_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
                ROS_INFO_NAMED("tutorial", "Visualizing plan 1 (pose goal) %s", success ? "" : "FAILED");


                ROS_INFO_NAMED("tutorial", "Visualizing plan 1 as trajectory line");
                visual_tools.publishAxisLabeled(target_pose1, "pose1");
                visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
                visual_tools.publishTrajectoryLine(move_plan.trajectory_, joint_model_group);
                visual_tools.trigger();
                visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");



                moveit::core::RobotStatePtr current_state = group.getCurrentState();
                std::vector<double> joint_group_positions;
                current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

                joint_group_positions[0] = -1.0;  // radians
                group.setJointValueTarget(joint_group_positions);

                success = (group.plan(move_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
                ROS_INFO_NAMED("tutorial", "Visualizing plan 2 (joint space goal) %s", success ? "" : "FAILED");

                visual_tools.deleteAllMarkers();
                visual_tools.publishText(text_pose, "Joint Space Goal", rvt::WHITE, rvt::XLARGE);
                visual_tools.publishTrajectoryLine(move_plan.trajectory_, joint_model_group);
                visual_tools.trigger();
                visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");
	}
		
		
		
